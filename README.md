Для запуска сервера postgresql
	sudo service postgresql start
	sudo -u postgres psql
	postgres=# ALTER USER postgres PASSWORD 'test_password';
	postgres=# \q
	psql -U postgres -h localhost
Для запуска сервиса
	Создать и запустить виртуальное окружение
	pip install -r requirements.txt

python app/manage.py init - добавление тестовых данных
python app/manage.py runserver - запуск сервера
python app/manage.py debug - запуск сервера в режиме 'debug' 
