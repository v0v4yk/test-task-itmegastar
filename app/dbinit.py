def insert_writer(conn, name):
    sql = '''INSERT INTO Writer (name) VALUES ({})'''.format(name)
    with conn.cursor() as cursor:
        cursor.execute(sql)


def insert_book(conn, author_id, book_name):
    sql = '''INSERT INTO Book (author_id, name) VALUES ({0}, {1})'''.format(author_id, book_name)
    with conn.cursor() as cursor:
        cursor.execute(sql, (author_id, book_name))


def create_tables(conn):
    with conn.cursor() as cursor:
        cursor.execute('''
            CREATE TABLE IF NOT EXISTS Writer
            (id SERIAL PRIMARY KEY,
            name TEXT NOT NULL)
            ''')
        cursor.execute('''
            CREATE TABLE IF NOT EXISTS Book (
            id    SERIAL PRIMARY KEY,
            author_id INTEGER NOT NULL,
            name  TEXT NOT NULL,
            FOREIGN KEY(author_id) REFERENCES Writer(id) ON DELETE CASCADE);
            ''')


def init(conn):
    create_tables(conn)
    insert_writer(conn, "'Толстой Лев'")
    insert_writer(conn, "'Пушкин Александр'")
    insert_book(conn, 1, "'Война и мир'")
    insert_book(conn, 1, "'Воскресенье'")
    insert_book(conn, 1, "'Анна Каренина'")
    insert_book(conn, 2, "'Евгений Онегин'")
    insert_book(conn, 2, "'Каптанская дочка'")
    insert_book(conn, 2, "'Пиковая дама'")
