class WriterModel():
    def __init__(self, conn):
        self.conn = conn

    def get_writer(self, author_id):
        sql = '''SELECT Writer.id, Writer.name, Book.id, Book.name
            FROM Writer LEFT JOIN Book on Book.author_id=Writer.id
            WHERE Book.author_id={0}'''.format(author_id)
        with self.conn.cursor() as cursor:
            cursor.execute(sql)
            rows = cursor.fetchall()
            if len(rows) > 0:
                writer = {
                    'id': rows[0][0],
                    'name': rows[0][1],
                    'books': [{'id': row[2], 'name': row[3]} for row in rows]
                }
                return writer
        return None
