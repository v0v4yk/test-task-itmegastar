from flask import Flask, g, jsonify, abort
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
import psycopg2
import sys
import dbinit
from models import WriterModel


app = Flask(__name__)


def db_connect():
    conn = psycopg2.connect(
        dbname='postgres',
        user='postgres',
        password='test_password',
        host='127.0.0.1',
        port='5432')
    conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    return conn


@app.before_request
def before_request():
    g.db = db_connect()


@app.after_request
def after_request(response):
    if g.db is not None:
        g.db.close()
    return response


@app.route('/writers/<int:writer_id>')
def get_writer_view(writer_id):
    writer_model = WriterModel(g.db)
    writer = writer_model.get_writer(writer_id)
    if writer is not None:
        return jsonify(writer)
    else:
        abort(404, "Can't find writer with id {0}".format(writer_id))


if __name__ == '__main__':
    for arg in sys.argv:
        if arg == 'init':
            connection = db_connect()
            dbinit.init(connection)
            connection.close()
        if arg == 'runserver':
            app.run()
        if arg == 'debug':
            app.run(debug=True)
